// conf.js


exports.config = {
  framework: 'jasmine',

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  },

  seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['protractor.spec001.js'],

  multiCapabilities: [{
    browserName: 'firefox'
  }, {
    browserName: 'chrome'
  }],

  onPrepare: function () {
    var jasmineReporters = require('jasmine-reporters');

    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      filePrefix: 'xmloutput',
      savePath: 'testresults'
    }));
  },


  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000,
    showColors: false
  }
};
