<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8" />
<title>ASF Cloud Ready</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/foundation.min.css" />
<link rel="stylesheet" href="css/app.css" />
<link rel="icon" href="img/favicon-150.png">


</head>
<body>
	<div class="title-bar" data-responsive-toggle="realEstateMenu"
		data-hide-for="small">
		<button class="menu-icon" type="button" data-toggle></button>
		<div class="title-bar-title">Menu</div>
	</div>
	<div class="top-bar" id="realEstateMenu">
		<div class="top-bar-left">
			<ul class="menu" data-responsive-menu="accordion">
				<li class="menu-text">Home</li>
				<li><a href="#">One</a></li>

			</ul>
		</div>
		<div class="top-bar-right">
			<ul class="menu">
				<li><a href="#">My Account</a></li>
			</ul>
		</div>
	</div>

	<br>
	<div class="row">
		<div class="medium-7 large-6 columns">
			<h2>CF Variables Binding</h2>
			<p class="subheader">List of Cloud Foundry variables</p>
		</div>

	</div>

	<div class="row">

		<div class="column">
			<div class="callout">
				<p>CF_HOME</p>
				<p>
					<code>
						<%=System.getenv("CF_HOME")%>
					</code>
				</p>

			</div>
			<div class="callout">
				<p>CF_INSTANCE_ADDR</p>
				<p>
					<code>
						<%=System.getenv("CF_INSTANCE_ADDR")%>
					</code>
				</p>

			</div>
			<div class="callout">
				<p>VCAP_SERVICES</p>
				<p>
					<code>
						<%=System.getenv("VCAP_SERVICES")%>
					</code>
				</p>

			</div>
			<div class="callout">
				<p>HOME</p>
				<p>
					<code>
						<%=System.getenv("HOME")%>
					</code>
				</p>

			</div>
		</div>

	</div>



	<div class="row column">
		<hr>
	</div>

	<footer>
		<div class="row">
			<div class="medium-6 columns">
				<ul class="menu">
					<li><a href="#">@khajour</a></li>

				</ul>
			</div>

		</div>
	</footer>

	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/what-input.min.js"></script>
	<script src="js/foundation.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>