#!/bin/bash
#
#  Setup wp-content on SSHFS
#   Author:  Daniel Mikusa <dmikusa@pivotal.io>
#     Date:  4/23/2015
#
#   Changed by Alexandre Vasseur (2016)
#     Make it generic and use of jq to parse VCAP_SERVICES on a cups
#
#   Changed by Didier Romelot (2016)
#     use of private key
set -eo pipefail

# Do not "exit 0" as this will also exit the container staging process

# move scripts out of public directory
echo ".profile.d/*.sh in action"
echo $HOME

# If there's an SSHFS user provided service, mount it
echo "Checking for VCAP_SERVICES sshfs"

if [ "$VCAP_SERVICES" != "{}" ]; then

	SSHFS_CUPS_NAME=$(echo $VCAP_SERVICES | jq 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].name')

	if [ "$SSHFS_CUPS_NAME" != "null" ]; then

		echo "Found SSHFS User Provided Service [$SSHFS_CUPS_NAME]"
	
		SSHFS_CUPS=$(echo $VCAP_SERVICES | jq 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].name' | grep -c "sshfs")

		# copy private key if provided by the app
		if [ -d $HOME/WEB-INF/.ssh ]; then
			echo "found .ssh in WEB-INF will copy content"
			mkdir /home/vcap/.ssh
			chmod 700 /home/vcap/.ssh/
			cp $HOME/WEB-INF/.ssh/* /home/vcap/.ssh
			chmod 600 /home/vcap/.ssh/*
		else
			echo "ERROR : No folder .ssh found in WEB-INF"
			echo "ERROR : must at least provide a private key in WEB-INF/.ssh folder"
		fi

		# if there's a known_hosts file provided, enable StrictHostKeyChecking
		if [ -f $HOME/.ssh/known_hosts ]; then
			chmod 644 $HOME/.ssh/known_hosts 
			SSHFS_OPTS="-o StrictHostKeyChecking=yes -o UserKnownHostsFile=$HOME/.ssh/known_hosts $SSHFS_OPTS"
		else
			SSHFS_OPTS="-o StrictHostKeyChecking=no $SSHFS_OPTS"
		fi

		# get credentials from the first bound sshfs service
		FS_HOST=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.host')
		FS_USER=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.username')
		FS_KEY=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.keyname')
		FS_PORT=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.port')
		FS_PATH=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.path')
		FS_REMOTE_PATH=$(echo $VCAP_SERVICES | jq -r 'to_entries|map(select(.key|contains("user-provided")))[0].value|map(select(.name|contains("sshfs")))[0].credentials.remotepath')
 
		echo "host       ["$FS_HOST"]"
		echo "username   ["$FS_USER"]"
		echo "keyname    ["$FS_KEY"]"
		echo "port       ["$FS_PORT"]"
		echo "path       ["$FS_PATH"]"
		echo "remotepath ["$FS_REMOTE_PATH"]"

		if [ "$FS_PATH" == "null" ]; then
			FS_PATH="/home/vcap/sharedfs"
		fi

		# create a directory where we can mount sshfs
		mkdir -p "$FS_PATH"

		# use sshfs to mount the remote filesystem
		sshfs "$FS_USER@$FS_HOST:$FS_REMOTE_PATH" \
			"$FS_PATH" \
			-o IdentityFile=home/vcap/.ssh/$FS_KEY \
			-o port=$FS_PORT \
			-o idmap=user \
			-o reconnect \
			-o compression=no  \
			-o sshfs_debug $SSHFS_OPTS
				
		# we're done
		echo "Done mounting SSHFS."
	else
		echo "No SSHFS in VCAP_SERVICES"
	fi
fi
