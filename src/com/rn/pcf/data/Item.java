package com.rn.pcf.data;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "item")

public class Item implements Serializable {

	@Id
	@NotNull
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long  id;

	private String name;
	private String reference;
	private int price;
	private String image;
	
	
	public Item(String name, String reference, int price, String image) {
		super();
		//this.id = 92886632;
		this.name = name;
		this.reference = reference;
		this.price = price;
		this.image = image;
	}

	public Item() {
		super();
	}

	private static final long serialVersionUID = 1L;
	public static String FIND_ALL = "";


	

	public Long  getId() {
		return id;
	}

	public void setId(Long  id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}