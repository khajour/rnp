package com.rn.pcf.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class ItemDAO {

	private EntityManagerFactory emf;
	
	private EntityManager em;
	private static final String SELCT_QUERY = "SELECT c FROM Item c ORDER BY c.id DESC";

	public ItemDAO() {
		emf = Persistence.createEntityManagerFactory("default");
		em = emf.createEntityManager();
		//em = PersistenceListener.createEntityManager();
		
	}

	public List<Item> findAll() {
		Query q1 = em.createQuery(SELCT_QUERY);
		q1.setFirstResult(0 * 100).setMaxResults(100);
		return q1.getResultList();
	}

	public Item addItem(Item item) {
		em.getTransaction().begin();
		em.persist(item);
		em.getTransaction().commit();
		return item;
	}

}