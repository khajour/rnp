package com.rn.pcf.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.rn.pcf.data.Item;

@Path("/items")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ItemEndpoint {

	@POST
	public Response create(final Item item) {
		//TODO: process the given item 
		//here we use Item#getId(), assuming that it provides the identifier to retrieve the created Item resource. 
		return Response.created(UriBuilder.fromResource(ItemEndpoint.class).path(String.valueOf(item.getId())).build())
				.build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	public Response findById(@PathParam("id") final Long id) {
		//TODO: retrieve the item 
		Item item = null;
		if (item == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(item).build();
	}

	@GET
	public List<Item> listAll(@QueryParam("start") final Integer startPosition,
			@QueryParam("max") final Integer maxResult) {
		//TODO: retrieve the items 
		final List<Item> items = null;
		return items;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	public Response update(@PathParam("id") Long id, final Item item) {
		//TODO: process the given item 
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") final Long id) {
		//TODO: process the item matching by the given id 
		return Response.noContent().build();
	}

}
