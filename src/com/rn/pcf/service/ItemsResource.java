package com.rn.pcf.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.rn.pcf.data.Item;
import com.rn.pcf.data.ItemDAO;

@Path("/parts")
@Consumes({"application/json" })
@Produces({"application/json" })

public class ItemsResource {

	@GET
	@Path("all")
	
	public List<Item> getItems() throws SQLException, NamingException {
		List<Item> res = new ArrayList<Item>();
		ItemDAO dao = new ItemDAO();
		res = dao.findAll();
		return res;
	}


	@POST
	@Path("add")
	public Item addPart() {
		Item item = new Item("Aziz", "HHDFGH-4435-FF", 126, "img000333.png");
		ItemDAO dao = new ItemDAO();
		Item res = dao.addItem(item);
		return res;
	}
	
}
